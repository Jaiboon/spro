using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopWeb.ApplicationCore.Exceptions;

namespace Web.Controllers
{
    [Route("[controller]/[action]")]
    public class NewsContrller : Controller
    {
        private readonly INewsService _newsService;
        public NewsContrller(INewsService newsService)
        {
            _newsService = newsService;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody]News Dto)
        {
            try
            {
                if (Dto.CreateDate == DateTime.MinValue || Dto.ModifyDate == DateTime.MinValue)
                {
                    Dto.CreateDate = DateTime.Now;
                    Dto.ModifyDate = DateTime.Now;
                }

                await _newsService.Create(Dto);
                return Ok();
            }
            catch (BasketNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> SaveAsync([FromBody]News Dto)
        {
            try
            {
                if (Dto.Id == 0)
                {
                    await Create(Dto);
                }
                else
                {
                    var res = await _newsService.Get(Dto.Id);

                    if (res == null)
                    {
                        return BadRequest("No data for edit.");
                    }

                    await Edit(Dto);
                }
                return Ok();
            }
            catch (BasketNotFoundException ex)
            {
                return NotFound(ex.Message); 
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public async Task<IList<News>> GetNews()
        {
            return await _newsService.GetNews();
        }

        [HttpGet]
        public async Task<News> Get(int id)
        {
            return await _newsService.Get(id);
        }

        [HttpPut]
        public async Task<IActionResult> Edit([FromBody]News Dto)
        {
            try
            {
                if (Dto.CreateDate == DateTime.MinValue || Dto.ModifyDate == DateTime.MinValue)
                {
                    Dto.CreateDate = DateTime.Now;
                    Dto.ModifyDate = DateTime.Now;
                }

                await _newsService.Edit(Dto);
                return Ok();
            }
            catch (BasketNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        public async Task Remove(int id)
        {
            try
            {
                var news = await _newsService.Get(id);
                if (news.Title != null)
                {
                    await _newsService.Delete(news);
                }
            }
            catch (Exception ex)
            {
                BadRequest(ex.Message);
            }
        }
    }
}