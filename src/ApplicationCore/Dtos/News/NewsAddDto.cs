namespace ApplicationCore.Dtos.News
{
    public class NewsAddDto
    {
        public string Title { get; set; }
        public string Content { get; set; }
    }
}