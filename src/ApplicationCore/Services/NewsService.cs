using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using Microsoft.eShopWeb.ApplicationCore.Entities.BasketAggregate;
using Microsoft.eShopWeb.ApplicationCore.Interfaces;

namespace ApplicationCore.Services
{
    public class NewsService : INewsService
    {
        private readonly IAsyncRepository<News> _newsRepository;
        public NewsService(IAsyncRepository<News> newsRepository)
        {
            _newsRepository = newsRepository;
        }
        public async Task Create(News Dto)
        {
            await _newsRepository.AddAsync(Dto);
        }

        public async Task Delete(News Dto)
        {
            await _newsRepository.DeleteAsync(Dto);
        }

        public async Task Edit(News Dto)
        {
            await _newsRepository.UpdateAsync(Dto);
        }

        public async Task<News> Get(int id)
        {
            var res = await _newsRepository.GetByIdAsync(id);
            return res;
        }

        public async Task<IList<News>> GetNews()
        {
            var res = await _newsRepository.ListAllAsync();
            return res;
        }
    }
}