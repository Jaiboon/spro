using Microsoft.eShopWeb.ApplicationCore.Entities;

namespace ApplicationCore.Entities
{
    public class News : BaseEntity
    {
        public string Title { get; set; }
        public string Content { get; set; }
    }
}