using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using Microsoft.eShopWeb.ApplicationCore.Interfaces;

namespace ApplicationCore.Interfaces
{
    public interface INewsService
    {
        Task<IList<News>> GetNews();
        Task<News> Get(int id);
        Task Create(News Dto);
        Task Edit(News Dto);
        Task Delete(News Dto);
    }
}